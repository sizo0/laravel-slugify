# Laravel Slugify Service Provider
[ ![Codeship Status for sizo0/laravel-slugify](https://codeship.com/projects/65b00690-bea4-0133-31cb-022ad975b6ca/status?branch=master)](https://codeship.com/projects/136962)

## Description
- Slugify strings
- [Laravel][laravel] Service Provider


## Installation
Install using [Composer][composer]
```
$ composer require sizo0/laravel-slugify
```

## Usage
### Outside Laravel
```php
$Slugify = new \Slugify\Slugify();
echo $Slugify->slugify("I am a string with àccénts"); // i-am-a-string-with-accents
```
### Inside Laravel
TODO

## Contributing
### Project structure
```
├───Slugify
├───tests
└───vendor
```
### How to ?
Please send an email to **Hassan El Omari Alaoui** at elomarialaoui.hassan@gmail.com.

## Authors
- [Hassan El Omari Alaoui][helomari_bitbucket]

## License
```
The MIT License (MIT)

Copyright (c) 2015 Hassan El Omari Alaoui <elomarialaoui.hassan@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```


[composer]: https://getcomposer.org/
[laravel]: http://laravel.com
[helomari_bitbucket]: https://bitbucket.org/sizo0