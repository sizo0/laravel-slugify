<?php
namespace SlugifyTest;


use PHPUnit_Framework_TestCase;
use Slugify\Slugify;

class SlugifyTest extends PHPUnit_Framework_TestCase
{

    /**
     * Slugify accents
     */
    public function testSlugifyAccents()
    {
        $string = "éàûü";
        $slugify = new Slugify();
        $slug = $slugify->slugify($string);

        $this->assertEquals("eauu", $slug);
    }

    /**
     * Slugify spaces
     */
    public function testSlugifySpaces()
    {
        $string = "I contain spaces";
        $slugify = new Slugify();
        $slug = $slugify->slugify($string);

        $this->assertEquals("i-contain-spaces", $slug);
    }

    /**
     * Slugify spaces at the beginning and the end of the string
     */
    public function testSlugifySpacesStartEnd()
    {
        $string = "   Start and end space  ";
        $slugify = new Slugify();
        $slug = $slugify->slugify($string);

        $this->assertEquals("start-and-end-space", $slug);
    }

    /**
     * Slugify other characters than letters or numbers
     */
    public function testSlugifyOtherCharacters()
    {
        $string = "It's a great n°";
        $slugify = new Slugify();
        $slug = $slugify->slugify($string);

        $this->assertEquals("its-a-great-n", $slug);
    }
}